
import VaccineProvider from '@/resources/vaccine-provider'

const vaccineService = new VaccineProvider()

const vaccine = {
    namespaced: true,
    state: {
        nameCriteria: null,
        createDateCriteria: null,
        pageCriteria: 1,
        vaccineBookList: [],
        vaccineBook: {},
        vaccineList: [],
        vaccine: {},
        totalRecords: null,
        isLoading: false
    },
    mutations: {
        SET_NAME_CRITERIA : (state, payload) => {
            state.nameCriteria = payload
        },
        SET_CREATE_DATE_CRITERIA : (state, payload) => {
            state.createDateCriteria = payload
        },
        SET_PAGE_CRITERIA : (state, payload) => {
            state.pageCriteria = payload
        },
        SET_VACCINE_BOOK_LIST: (state, payload) => {
            state.vaccineBookList = payload
        },
        SET_VACCINE_BOOK: (state, payload) => {
            state.vaccineBook = payload
        },
        SET_VACCINE_LIST: (state, payload) => {
            state.vaccineList = payload
        },
        SET_VACCINE: (state, payload) => {
            state.vaccine = payload
        },
        SET_TOTAL_RECORDS: (state, payload) => {
            state.totalRecords = payload
        },
        SET_IS_LOADING: (state, payload) => {
            state.isLoading = payload
        }
    },
    actions: {
        GET_VACCINE_BOOK_LIST: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                vaccineService.getVaccineBookList(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_VACCINE_BOOK_LIST', data.result.items)
                        commit('SET_TOTAL_RECORDS', data.result.totalRecords)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_VACCINE_BOOK_LIST', [])
                        commit('SET_TOTAL_RECORDS', 0)
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_VACCINE_BOOK_LIST', [])
                    commit('SET_TOTAL_RECORDS', 0)
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_VACCINE_BOOK_LIST_BY_CREATURE_ID: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                vaccineService.getVaccineBookListByCreatureId(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_VACCINE_BOOK_LIST', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_VACCINE_BOOK_LIST', [])
                        commit('SET_TOTAL_RECORDS', 0)
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_VACCINE_BOOK_LIST', [])
                    commit('SET_TOTAL_RECORDS', 0)
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_VACCINE_BOOK_BY_ID: ({commit, dispatch}, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                vaccineService.getVaccineBookById(id).then(v => {
                    const { status, data, errorMessage } = v

                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_VACCINE_BOOK', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('SET_VACCINE_BOOK', null)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    commit('SET_VACCINE_BOOK', null)
                    reject(error.message)
                })
            })
        },
        UPDATE_VACCINE_BOOK: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                vaccineService.updateVaccineBook(data).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        DELETE_VACCINE_BOOK: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                vaccineService.deleteVaccineBook(payload).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        /////////////////////// VACCINE ///////////////////////
        GET_VACCINE_LIST_BY_BOOK_ID: ({commit, dispatch}, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                vaccineService.getVaccineListByBookId(id).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_VACCINE_LIST', data.result.items)
                        commit('SET_IS_LOADING', false)
                        resolve(data.result)
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('SET_VACCINE_LIST', [])
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    commit('SET_VACCINE_LIST', [])
                    reject(error.message)
                })
            })
        },
        GET_VACCINE_LIST: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                vaccineService.getVaccineList(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_VACCINE_LIST', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve(data.result)
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('SET_VACCINE_LIST', [])
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    commit('SET_VACCINE_LIST', [])
                    reject(error.message)
                })
            })
        },
        GET_VACCINE_BY_ID: ({commit, dispatch}, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                vaccineService.getVaccineById(id).then(v => {
                    const { status, data, errorMessage } = v

                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_VACCINE', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('SET_VACCINE', null)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    commit('SET_VACCINE', null)
                    reject(error.message)
                })
            })
        },
        UPDATE_VACCINE: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                vaccineService.updateVaccine(data).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        DELETE_VACCINE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                vaccineService.deleteVaccine(payload).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        }
        
    },
    getters: {
        vaccineBookList: state => state.vaccineBookList,
        vaccineBook: state => state.vaccineBook,
        vaccineList: state => state.vaccineList,
        vaccine: state => state.vaccine
    }
}

export default vaccine