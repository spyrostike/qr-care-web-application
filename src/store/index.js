import Vue from 'vue'
import Vuex from 'vuex'
import app from './app'
import admin from './admin'
import creature from './creature'
import drug from './drug'
import drugAllergy from './drug-allergy'
import file from './file'
import foodAllergy from './food-allergy'
import medical from './medical'
import member from './member'
import qrcode from './qrcode'
import treatmentHistory from './treatment-history'
import vaccine from './vaccine'

Vue.use(Vuex)

const modules = {
    app: app,
    admin: admin,
    creature: creature,
    drug: drug,
    drugAllergy: drugAllergy,
    file: file,
    foodAllergy: foodAllergy,
    medical: medical,
    member: member,
    qrcode: qrcode,
    treatmentHistory: treatmentHistory,
    vaccine: vaccine
}

export default new Vuex.Store({
    modules
})
