import QRCodeProvider from '@/resources/qrcode-provider'

const qrcodeService = new QRCodeProvider()

const admin = {
    namespaced: true,
    state: {
        serialNoCriteria: null,
        setCreateDateCriteria: null,
        nameCriteria: null,
        createDateCriteria: null,
        pageCriteria: 1,
        qrcodeSet: {},
        qrcodeSetList: [],
        qrcodeItem: {},
        qrcodeItemList: [],
        isLoading: false,
        totalRecords: null,
        totaQRCodeItemlRecords: null,
        pageQRCodeItemCriteria: 1
    },
    mutations: {
        SET_SERIAL_NO_CRITERIA : (state, payload) => {
            state.serialNoCriteria = payload
        },
        SET_ฺNO_CRITERIA : (state, payload) => {
            state.setCreateDateCriteria = payload
        },
        SET_NAME_CRITERIA : (state, payload) => {
            state.nameCriteria = payload
        },
        SET_ฺCREATE_DATE_CRITERIA : (state, payload) => {
            state.createDateCriteria = payload
        },
        SET_PAGE_CRITERIA : (state, payload) => {
            state.pageCriteria = payload
        },
        SET_QRCODE_SET_LIST : (state, payload) => {
            state.qrcodeSetList = payload
        },
        SET_QRCODE_SET_LIST : (state, payload) => {
            state.qrcodeSetList = payload
        },
        SET_IS_LOADING: (state, payload) => {
            state.isLoading = payload
        },
        SET_TOTAL_RECORDS: (state, payload) => {
            state.totalRecords = payload
        },
        SET_QRCODE_ITEM_TOTAL_RECORDS: (state, payload) => {
            state.totaQRCodeItemlRecords = payload
        },
        SET_QRCODE_SET: (state, payload) => {
            state.qrcodeSet = payload
        },
        SET_QRCODE_ITEM_LIST: (state, payload) => {
            state.qrcodeItemList = payload
        },
        SET_QRCODE_ITEM: (state, payload) => {
            state.qrcodeItem = payload
        },
        SET_PAGE_QRCODE_ITEM_CRITERIA : (state, payload) => {
            state.pageQRCodeItemCriteria = payload
        }
    },
    actions: {
        QRCODE_GENERATE: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                qrcodeService.generate(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        
                        resolve()
                    } else {
                        reject(errorMessage)
                    }

                }).catch(error => {
                    reject(error.message)
                })
            })
        },
        GET_QRCODE_SET_LIST: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                qrcodeService.getQRCodeSetList(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_QRCODE_SET_LIST', data.result.items)
                        commit('SET_TOTAL_RECORDS', data.result.totalRecords)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_QRCODE_SET_LIST', [])
                        commit('SET_TOTAL_RECORDS', 0)
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_QRCODE_SET_LIST', [])
                    commit('SET_TOTAL_RECORDS', 0)
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_QRCODE_ITEM_LIST: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                qrcodeService.getQRCodeItemList(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_QRCODE_ITEM_LIST', data.result.items)
                        commit('SET_QRCODE_ITEM_TOTAL_RECORDS', data.result.totalRecords)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_QRCODE_ITEM_LIST', [])
                        commit('SET_QRCODE_ITEM_TOTAL_RECORDS', 0)
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_QRCODE_ITEM_LIST', [])
                    commit('SET_QRCODE_ITEM_TOTAL_RECORDS', 0)
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_QRCODE_SET_BY_ID: ({commit, dispatch}, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                qrcodeService.getQRCodeSetById(id).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_QRCODE_SET', data.result)
                        // commit('SET_QRCODE_ITEM_LIST', data.result.items)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('SET_QRCODE_ITEM_LIST', [])
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    commit('SET_QRCODE_ITEM_LIST', [])
                    reject(error.message)
                })
            })
        },
        DOWNLOAD_PDF: ({commit, dispatch}, id) => {
            return new Promise((resolve, reject) => { 
                qrcodeService.downloadPDF(id).then(v => {
                    resolve(v)
                }).catch(error => {
                    reject(error.message)
                })
            })
        },
        UPDATE_QRCODE_SET: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                qrcodeService.updateQRCodeSet(data).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
                
            })
        },
        GET_QRCODE_INFO: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                qrcodeService.getQrcodeInfo().then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve(data.result)
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
                
            })
        }
    },
    getters: {
        qrcodeSet: state => state.qrcodeSet,
        qrcodeSetList: state => state.qrcodeSetList,
        qrcodeItem: state => state.qrcodeItem,
        qrcodeItemList: state => state.qrcodeItemList
    }
}

export default admin