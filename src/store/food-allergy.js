
import FoodAllergyProvider from '@/resources/food-allergy-provider'

const foodAllergyService = new FoodAllergyProvider()

const foodAllergy = {
    namespaced: true,
    state: {
        nameCriteria: null,
        createDateCriteria: null,
        pageCriteria: 1,
        foodAllergy: {},
        foodAllergyList: [],
        totalRecords: null,
        isLoading: false
    },
    mutations: {
        SET_NAME_CRITERIA : (state, payload) => {
            state.nameCriteria = payload
        },
        SET_CREATE_DATE_CRITERIA : (state, payload) => {
            state.createDateCriteria = payload
        },
        SET_PAGE_CRITERIA : (state, payload) => {
            state.pageCriteria = payload
        },
        SET_FOOD_ALLERGY_LIST : (state, payload) => {
            state.foodAllergyList = payload
        },
        SET_FOOD_ALLERGY: (state, payload) => {
            state.foodAllergy = payload
        },
        SET_TOTAL_RECORDS: (state, payload) => {
            state.totalRecords = payload
        },
        SET_IS_LOADING: (state, payload) => {
            state.isLoading = payload
        }
    },
    actions: {
        GET_FOOD_ALLERGY_LIST: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                foodAllergyService.getFoodAllergyList(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_FOOD_ALLERGY_LIST', data.result.items)
                        commit('SET_TOTAL_RECORDS', data.result.totalRecords)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_FOOD_ALLERGY_LIST', [])
                        commit('SET_TOTAL_RECORDS', 0)
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_FOOD_ALLERGY_LIST', [])
                    commit('SET_TOTAL_RECORDS', 0)
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_FOOD_ALLERGY_LIST_BY_CREATURE_ID: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                foodAllergyService.getFoodAllergyListByCreatureId(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_FOOD_ALLERGY_LIST', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_FOOD_ALLERGY_LIST', [])
                        commit('SET_TOTAL_RECORDS', 0)
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_FOOD_ALLERGY_LIST', [])
                    commit('SET_TOTAL_RECORDS', 0)
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_FOOD_ALLERGY_BY_ID: ({commit, dispatch}, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                foodAllergyService.getFoodAllergyById(id).then(v => {
                    const { status, data, errorMessage } = v

                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_FOOD_ALLERGY', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('SET_FOOD_ALLERGY', null)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    commit('SET_FOOD_ALLERGY', null)
                    reject(error.message)
                })
            })
        },
        UPDATE_FOOD_ALLERGY: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                foodAllergyService.updateFoodAllergy(data).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        DELETE_FOOD_ALLERGY: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                foodAllergyService.deleteFoodAllergy(payload).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        COUNT_FOOD_ALLERGY_BY_CREATURE_ID: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                foodAllergyService.countFoodAllergyByCreatureId(payload).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        commit('SET_TOTAL_RECORDS', data.result)
                        resolve(data.result)
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        }
    },
    getters: {
        foodAllergy: state => state.foodAllergy,
        foodAllergyList: state => state.foodAllergyList,
    }
}

export default foodAllergy