
import DrugProvider from '@/resources/drug-provider'

const drugService = new DrugProvider()

const drug = {
    namespaced: true,
    state: {
        nameCriteria: null,
        createDateCriteria: null,
        pageCriteria: 1,
        drug: {},
        drugList: [],
        totalRecords: null,
        isLoading: false
    },
    mutations: {
        SET_NAME_CRITERIA : (state, payload) => {
            state.nameCriteria = payload
        },
        SET_CREATE_DATE_CRITERIA : (state, payload) => {
            state.createDateCriteria = payload
        },
        SET_PAGE_CRITERIA : (state, payload) => {
            state.pageCriteria = payload
        },
        SET_DRUG_LIST : (state, payload) => {
            state.drugList = payload
        },
        SET_DRUG: (state, payload) => {
            state.drug = payload
        },
        SET_TOTAL_RECORDS: (state, payload) => {
            state.totalRecords = payload
        },
        SET_IS_LOADING: (state, payload) => {
            state.isLoading = payload
        }
    },
    actions: {
        GET_DRUG_LIST: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                drugService.getDrugList(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_DRUG_LIST', data.result.items)
                        commit('SET_TOTAL_RECORDS', data.result.totalRecords)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_DRUG_LIST', [])
                        commit('SET_TOTAL_RECORDS', 0)
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_DRUG_LIST', [])
                    commit('SET_TOTAL_RECORDS', 0)
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_DRUG_LIST_BY_CREATURE_ID: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                drugService.getDrugListByCreatureId(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_DRUG_LIST', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_DRUG_LIST', [])
                        commit('SET_TOTAL_RECORDS', 0)
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_DRUG_LIST', [])
                    commit('SET_TOTAL_RECORDS', 0)
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_DRUG_BY_ID: ({commit, dispatch}, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                drugService.getDrugById(id).then(v => {
                    const { status, data, errorMessage } = v

                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_DRUG', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('SET_DRUG', null)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    commit('SET_DRUG', null)
                    reject(error.message)
                })
            })
        },
        UPDATE_DRUG: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                drugService.updateDrug(data).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        DELETE_DRUG: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                drugService.deleteDrug(payload).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        COUNT_DRUG_BY_CREATURE_ID: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                drugService.countDrugByCreatureId(payload).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        commit('SET_TOTAL_RECORDS', data.result)
                        resolve(data.result)
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        }
    },
    getters: {
        drug: state => state.drug,
        drugList: state => state.drugList,
    }
}

export default drug