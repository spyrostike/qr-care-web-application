
import MedicalProvider from '@/resources/medical-provider'

const medicalService = new MedicalProvider()

const medical = {
    namespaced: true,
    state: {
        medical: {},
        totalRecords: null,
        isLoading: false
    },
    mutations: {
        SET_TEXT_CRITERIA : (state, payload) => {
            state.textCriteria = payload
        },
        SET_DATE_CRITERIA : (state, payload) => {
            state.dateCriteria = payload
        },
        SET_PAGE_CRITERIA : (state, payload) => {
            state.pageCriteria = payload
        },
        SET_MEDICAL: (state, payload) => {
            state.medical = payload
        },
        SET_TOTAL_RECORDS: (state, payload) => {
            state.totalRecords = payload
        },
        SET_IS_LOADING: (state, payload) => {
            state.isLoading = payload
        }
    },
    actions: {
        GET_MEDICAL_BY_CREATURE_ID: ({commit, dispatch}, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                medicalService.getMedicalByCreatureId(id).then(v => {
                    const { status, data, errorMessage } = v

                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_MEDICAL', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('SET_MEDICAL', null)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    commit('SET_MEDICAL', null)
                    reject(error.message)
                })
            })
        },
        UPDATE_MEDICAL: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                medicalService.updateMedical(data).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        }
    },
    getters: {
        medical: state => state.medical
    }
}

export default medical