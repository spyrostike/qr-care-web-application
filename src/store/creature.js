
import CreatureProvider from '@/resources/creature-provider'

const creatureService = new CreatureProvider()

const creature = {
    namespaced: true,
    state: {
        serialNoCriteria: null,
        nameCriteria: null,
        birthDateCriteria: null,
        pageCriteria: 1,
        creature: {},
        creatureList: [],
        totalRecords: null,
        isLoading: false
    },
    mutations: {
        SET_SERIAL_NO_CRITERIA : (state, payload) => {
            state.serialNoCriteria = payload
        },
        SET_NAME_CRITERIA : (state, payload) => {
            state.nameCriteria = payload
        },
        SET_ฺBIRTH_DATE_CRITERIA : (state, payload) => {
            state.birthDateCriteria = payload
        },
        SET_PAGE_CRITERIA : (state, payload) => {
            state.pageCriteria = payload
        },
        SET_CREATURE_LIST : (state, payload) => {
            state.creatureList = payload
        },
        SET_CREATURE: (state, payload) => {
            state.creature = payload
        },
        SET_TOTAL_RECORDS: (state, payload) => {
            state.totalRecords = payload
        },
        SET_IS_LOADING: (state, payload) => {
            state.isLoading = payload
        }
    },
    actions: {
        GET_CREATURE_LIST: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                creatureService.getCreatureList(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_CREATURE_LIST', data.result.items)
                        commit('SET_TOTAL_RECORDS', data.result.totalRecords)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_CREATURE_LIST', [])
                        commit('SET_TOTAL_RECORDS', 0)
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_CREATURE_LIST', [])
                    commit('SET_TOTAL_RECORDS', 0)
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_CREATURE_BY_ID: ({commit, dispatch}, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                creatureService.getCreatureById(id).then(v => {
                    const { status, data, errorMessage } = v

                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_CREATURE', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('SET_CREATURE', null)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    commit('SET_CREATURE', null)
                    reject(error.message)
                })
            })
        },
        GET_CREATURE_BY_QRCODE_ID: ({commit, dispatch}, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                creatureService.getCreatureByQRCodeId(id).then(v => {
                    const { status, data, errorMessage } = v

                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_CREATURE', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('SET_CREATURE', null)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    commit('SET_CREATURE', null)
                    reject(error.message)
                })
            })
        },
        UPDATE_CREATURE: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                creatureService.updateCreature(data).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        DELETE_CREATURE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                creatureService.deleteCreature(payload).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        }
    },
    getters: {
        creature: state => state.creature,
        creatureList: state => state.creatureList,
    }
}

export default creature