
import TreatmentHistoryProvider from '@/resources/treatment-history-provider'

const TreatmentHistoryService = new TreatmentHistoryProvider()

const treatmentHistory = {
    namespaced: true,
    state: {
        departmentCriteria: null,
        fileCategoryCriteria: null,
        createDateCriteria: null,
        pageCriteria: 1,
        treatmentHistory: {},
        treatmentHistoryList: [],
        totalRecords: null,
        isLoading: false
    },
    mutations: {
        SET_DEPARTMENT_CRITERIA : (state, payload) => {
            state.departmentCriteria = payload
        },
        SET_CREATE_DATE_CRITERIA : (state, payload) => {
            state.fileCategoryCriteria = payload
        },
        SET_CREATE_DATE_CRITERIA : (state, payload) => {
            state.createDateCriteria = payload
        },
        SET_DATE_CRITERIA : (state, payload) => {
            state.dateCriteria = payload
        },
        SET_PAGE_CRITERIA : (state, payload) => {
            state.pageCriteria = payload
        },
        SET_TREATMENT_HISTORY_LIST : (state, payload) => {
            state.treatmentHistoryList = payload
        },
        SET_TREATMENT_HISTORY: (state, payload) => {
            state.treatmentHistory = payload
        },
        SET_TOTAL_RECORDS: (state, payload) => {
            state.totalRecords = payload
        },
        SET_IS_LOADING: (state, payload) => {
            state.isLoading = payload
        }
    },
    actions: {
        GET_TREATMENT_HISTORY_LIST: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                TreatmentHistoryService.getTreatmentHistoryList(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_TREATMENT_HISTORY_LIST', data.result.items)
                        commit('SET_TOTAL_RECORDS', data.result.totalRecords)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_TREATMENT_HISTORY_LIST', [])
                        commit('SET_TOTAL_RECORDS', 0)
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_TREATMENT_HISTORY_LIST', [])
                    commit('SET_TOTAL_RECORDS', 0)
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_TREATMENT_HISTORY_LIST_BY_CREATURE_ID: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                TreatmentHistoryService.getTreatmentHistoryListByCreatureId(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_TREATMENT_HISTORY_LIST', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_TREATMENT_HISTORY_LIST', [])
                        commit('SET_TOTAL_RECORDS', 0)
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_TREATMENT_HISTORY_LIST', [])
                    commit('SET_TOTAL_RECORDS', 0)
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_TREATMENT_HISTORY_BY_ID: ({commit, dispatch}, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                TreatmentHistoryService.getTreatmentHistoryById(id).then(v => {
                    const { status, data, errorMessage } = v

                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_TREATMENT_HISTORY', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('SET_TREATMENT_HISTORY', null)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    commit('SET_TREATMENT_HISTORY', null)
                    reject(error.message)
                })
            })
        },
        UPDATE_TREATMENT_HISTORY: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                TreatmentHistoryService.updateTreatmentHistory(data).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        DELETE_TREATMENT_HISTORY: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                TreatmentHistoryService.deleteTreatmentHistory(payload).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        }
    },
    getters: {
        treatmentHistory: state => state.treatmentHistory,
        treatmentHistoryList: state => state.treatmentHistoryList,
    }
}

export default treatmentHistory