
import FileProvider from '@/resources/file-provider'
import ObjectUtil from '@/utils/ObjectUtil'

const fileService = new FileProvider()

export default {
    namespaced: true,
    state: {
        isLoading: false
    },
    mutations: {
        SET_IS_LOADING: (state, payload) => {
            state.isLoading = payload
        }
    },
    actions: {
        ADD_FILE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                const data = ObjectUtil.genenrateFileObjToServer(payload)
                console.log('data', data)
                fileService.addFile(data).then(v => {
                    const { status, data, errorMessage } = v

                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve(data.result)
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }

                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_FILE_LIST: ({ commit, dispatch }, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                fileService.getFileList(id).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve(data.result)
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        DELETE_FILE: ({ commit, dispatch }, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                fileService.removeFile(id).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve(data.result)
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        }
    }
}