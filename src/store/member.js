
import MemberProvider from '@/resources/member-provider'

const memberService = new MemberProvider()

const admin = {
    namespaced: true,
    state: {
        usernameCriteria: null,
        nameCriteria: null,
        birthDateCriteria: null,
        pageCriteria: 1,
        item: {},
        list: [],
        isLoading: false,
        totalRecords: null
    },
    mutations: {
        SET_USERNAME_CRITERIA : (state, payload) => {
            state.usernameCriteria = payload
        },
        SET_NAME_CRITERIA : (state, payload) => {
            state.nameCriteria = payload
        },
        SET_ฺBIRTH_DATE_CRITERIA : (state, payload) => {
            state.birthDateCriteria = payload
        },
        SET_PAGE_CRITERIA : (state, payload) => {
            state.pageCriteria = payload
        },
        SET_MEMBER_LIST : (state, payload) => {
            state.list = payload
        },
        SET_MEMBER_ITEM: (state, payload) => {
            state.item = payload
        },
        SET_IS_LOADING: (state, payload) => {
            state.isLoading = payload
        },
        SET_TOTAL_RECORDS: (state, payload) => {
            state.totalRecords = payload
        }
    },
    actions: {
        GET_MEMBER_LIST: ({ commit, dispatch }, data) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                memberService.getMemberList(data).then(v => {
                    const { status, data, errorMessage } = v
                    
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_MEMBER_LIST', data.result.items)
                        commit('SET_TOTAL_RECORDS', data.result.totalRecords)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_MEMBER_LIST', [])
                        commit('SET_TOTAL_RECORDS', 0)
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_MEMBER_LIST', [])
                    commit('SET_TOTAL_RECORDS', 0)
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        GET_MEMBER_BY_ID: ({ commit, dispatch }, id) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                memberService.getMemberById(id).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_MEMBER_ITEM', data.result)
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                    
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        UPDATE_MEMBER: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)

                memberService.updateMember(payload).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        DELETE_MEMBER: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                memberService.deleteMember(payload).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        },
        CHANGE_PASSWORD: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                memberService.changePassword(payload).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        }
    },
    getters: {
        member: state => state.item,
        memberList: state => state.list,
    }
}

export default admin