
import AdminProvider from '@/resources/admin-provider'

const adminService = new AdminProvider()

const admin = {
    namespaced: true,
    state: {
        token: JSON.parse(localStorage.getItem(process.env.VUE_APP_ADMIN_TOKEN_NAME)) || null,
        status: null,
        isLoading: false,
        profile: {}
    },
    mutations: {
        AUTH_REQUEST : (state) => {
            state.status = 'loading'
            state.isLoading = true
        },
        AUTH_SUCCESS: (state, token) => {
            state.status = 'success'
            state.token = token
        },
        AUTH_ERROR: (state) => {
            state.status = 'error'
        },
        AUTH_LOGOUT: (state) => {
            state.token = null
        },
        SET_PROFILE: (state, payload) => {
            state.profile = payload
        }
    },
    actions: {
        AUTH_LOGIN: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                adminService.authLogin(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        localStorage.setItem(process.env.VUE_APP_ADMIN_TOKEN_NAME, JSON.stringify(data))
                        commit('AUTH_SUCCESS', data)
                        resolve()
                    } else {
                        localStorage.removeItem(process.env.VUE_APP_ADMIN_TOKEN_NAME)
                        commit('AUTH_ERROR')
                        reject(errorMessage)
                    }

                }).catch(error => {
                    localStorage.removeItem(process.env.VUE_APP_ADMIN_TOKEN_NAME)
                    reject(error.message)
                })
            })
        },
        AUTH_LOGOUT: ({commit, dispatch}) => {
            return new Promise((resolve, reject) => {
                commit('AUTH_LOGOUT')
                localStorage.removeItem(process.env.VUE_APP_ADMIN_TOKEN_NAME)
                // remove the axios default header
                // delete axios.defaults.headers.common['Authorization']
                resolve()
            })
        },
        GET_PROFILE:  ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => {
                adminService.getProfile(data).then(v => {
                    const { status, data, errorMessage } = v

                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_PROFILE', data.result)
                        resolve()
                    } else {
                        reject(errorMessage)
                    }

                }).catch(err => {
                    reject(err.message)
                })
            })
        },
        UPDATE_PROFILE: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                adminService.updateProfile(data).then(v => {
                    const { status, data, errorMessage } = v
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_PROFILE', data)
                        resolve()
                    } else {
                        reject(errorMessage)
                    }

                }).catch(error => {
                    localStorage.removeItem(process.env.VUE_APP_ADMIN_TOKEN_NAME)
                    reject(error.message)
                })
            })
        },
        CHANGE_PASSWORD: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                
                adminService.changePassword(payload).then(v => {
                    const { status, data, errorMessage } = v
                
                    if (status === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        reject(errorMessage)
                    }
                }).catch(error => {
                    commit('SET_IS_LOADING', false)
                    reject(error.message)
                })
            })
        }
    },
    getters: {
        isAuthenticated: state => !!state.token,
        authStatus: state => state.status
    }
}

export default admin