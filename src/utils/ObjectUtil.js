const genenrateFileObjToServer = (payload) => {
    var formData = new FormData()

    if (payload.id) { formData.append('id', payload.id) }
    if (payload.refId) { formData.append('refId', payload.refId) }
    formData.append('tag', payload.tag)
    formData.append('type', 'image')

    formData.append('file-data', payload.uri, payload.name)

    return formData
}

const genenrateMemberObjToServer = (member) => {
    var formData = new FormData()

    formData.append('id', member.id)
    formData.append('firstName', member.firstName)
    formData.append('lastName', member.lastName)
    formData.append('gender', member.gender)
    formData.append('bloodType', member.bloodType)
    formData.append('birthDate', member.birthDate)
    formData.append('relationship', member.relationship)
    formData.append('identificationNo', member.identificationNo)
    formData.append('contactNo1', member.contactNo1)
    formData.append('contactNo2', member.contactNo2)
    formData.append('address1', member.address1)
    formData.append('address2', member.address2)
    formData.append('address3', member.address3)
    formData.append('status', member.status)
    
    if (member.profileImage) formData.append('images', member.profileImage, 'profileImage.jpg')

    if (member.placeImage) formData.append('images', member.placeImage, 'placeImage.jpg')
    
    // console.log('images',formData.get('images')[0].name)
    // formData.forEach((value, key) => {
    //     console.log("key %s: value %s", key, value);
    // })

    return formData

}

export default {
    genenrateFileObjToServer,
    genenrateMemberObjToServer
}