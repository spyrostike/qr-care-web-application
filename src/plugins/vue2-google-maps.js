import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

/* eslint-disable eol-last */
Vue.use(VueGoogleMaps, {
  installComponents: true,
  load: {
    key: 'AIzaSyCNG7XFp5cxC__C9Fm31pld4-X53yGdVdk',
    libraries: 'places'
  }
})
