import HttpRequest from './http-request'

class AuthenticationProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    async authLogin (data) {
        return new Promise((resolve, reject) => {
                this.request('POST', '/admin/authen', data).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    async getProfile () {
        return new Promise((resolve, reject) => {
                this.get('/admin/profile').then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateProfile (data) {
        return new Promise((resolve, reject) => {
                this.update('/admin/update', data).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    changePassword = payload => {
        return new Promise((resolve, reject) => {
            this.update(`/admin/change-password`, payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default AuthenticationProvider