import HttpRequest from './http-request'

class QRCodeItemProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    addFile = payload => {
        return new Promise((resolve, reject) => {
                this.create('/file/add', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getFileList = payload => {
        return new Promise((resolve, reject) => {
                this.get('/file/get-file-list', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeFile = payload => {
        return new Promise((resolve, reject) => {
                this.delete(`/file/${payload}` ).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeFiles = payload => {
        return new Promise((resolve, reject) => {
                this.update(`/file/delete`, payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default QRCodeItemProvider