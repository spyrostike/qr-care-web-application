import HttpRequest from './http-request'

class DrugProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    getDrugList(data) {
        return new Promise((resolve, reject) => {
            this.get('/drug/get-drug-list', data).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getDrugListByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/drug/list/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
    
    getDrugById(id) {
        return new Promise((resolve, reject) => {
            this.get(`/drug/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateDrug(payload) {
        return new Promise((resolve, reject) => {
            this.update('/drug/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    deleteDrug(payload) {
        return new Promise((resolve, reject) => {
            this.delete(`/drug/delete/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    countDrugByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/drug/count/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

export default DrugProvider