import HttpRequest from './http-request'

class AuthenticationProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    getMemberList(payload) {
        return new Promise((resolve, reject) => {
            this.get('/member/get-member-list', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getMemberById(id) {
        return new Promise((resolve, reject) => {
            this.get(`/member/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateMember(payload) {
        return new Promise((resolve, reject) => {
            this.update('/member/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    deleteMember(payload) {
        return new Promise((resolve, reject) => {
            this.delete(`/member/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    changePassword = payload => {
        return new Promise((resolve, reject) => {
            this.update(`/member/change-password-member`, payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

export default AuthenticationProvider