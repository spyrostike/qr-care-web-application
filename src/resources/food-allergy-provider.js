import HttpRequest from './http-request'

class FoodAllergyProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    getFoodAllergyList(data) {
        return new Promise((resolve, reject) => {
            this.get('/food-allergy/get-food-allergy-list', data).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getFoodAllergyListByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/food-allergy/list/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getFoodAllergyById(id) {
        return new Promise((resolve, reject) => {
            this.get(`/food-allergy/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateFoodAllergy(payload) {
        return new Promise((resolve, reject) => {
            this.update('/food-allergy/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    deleteFoodAllergy(payload) {
        return new Promise((resolve, reject) => {
            this.delete(`/food-allergy/delete/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    countFoodAllergyByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/food-allergy/count/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

export default FoodAllergyProvider