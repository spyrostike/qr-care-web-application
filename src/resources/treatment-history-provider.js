import HttpRequest from './http-request'

class TreatmentHistoryProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    getTreatmentHistoryList(data) {
        return new Promise((resolve, reject) => {
            this.get('/treatment-history/get-treatment-history-list', data).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getTreatmentHistoryById(id) {
        return new Promise((resolve, reject) => {
            this.get(`/treatment-history/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateTreatmentHistory(payload) {
        return new Promise((resolve, reject) => {
            this.update('/treatment-history/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    deleteTreatmentHistory(payload) {
        return new Promise((resolve, reject) => {
            this.delete(`/treatment-history/delete/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getTreatmentHistoryListByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/treatment-history/list/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
    
}

export default TreatmentHistoryProvider