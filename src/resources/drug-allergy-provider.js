import HttpRequest from './http-request'

class DrugAllergyProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    getDrugAllergyList(data) {
        return new Promise((resolve, reject) => {
            this.get('/drug-allergy/get-drug-allergy-list', data).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getDrugAllergyListByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/drug-allergy/list/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getDrugAllergyById(id) {
        return new Promise((resolve, reject) => {
            this.get(`/drug-allergy/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateDrugAllergy(payload) {
        return new Promise((resolve, reject) => {
            this.update('/drug-allergy/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    deleteDrugAllergy(payload) {
        return new Promise((resolve, reject) => {
            this.delete(`/drug-allergy/delete/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    countDrugAllergyByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/drug-allergy/count/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

export default DrugAllergyProvider