import HttpRequest from './http-request'

class MedicalProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }z

    getMedicalByCreatureId(id) {
        return new Promise((resolve, reject) => {
            this.get(`/medical/get-medical-by-creature-id/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateMedical(payload) {
        return new Promise((resolve, reject) => {
            this.update('/medical/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

export default MedicalProvider