import HttpRequest from './http-request'

class CreatureProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    getCreatureList(data) {
        return new Promise((resolve, reject) => {
            this.get('/creature/get-creature-list', data).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getCreatureById(id) {
        return new Promise((resolve, reject) => {
            this.get(`/creature/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getCreatureByQRCodeId(id) {
        return new Promise((resolve, reject) => {
            this.get(`/creature/creature-by-qrcode-id/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateCreature(payload) {
        return new Promise((resolve, reject) => {
            this.update('/creature/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    deleteCreature(payload) {
        return new Promise((resolve, reject) => {
            this.delete(`/creature/delete/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

export default CreatureProvider