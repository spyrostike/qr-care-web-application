import HttpRequest from './http-request'

class VaccineProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    getVaccineBookList(payload) {
        return new Promise((resolve, reject) => {
            this.get('/vaccine/book/get-vaccine-book-list', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getVaccineBookById(id) {
        return new Promise((resolve, reject) => {
            this.get(`/vaccine/book/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateVaccineBook(payload) {
        return new Promise((resolve, reject) => {
            this.update('/vaccine/book/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    deleteVaccineBook(payload) {
        return new Promise((resolve, reject) => {
            this.delete(`/vaccine/book/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getVaccineBookListByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/vaccine/book/list/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    /////////////////////// VACCINE ///////////////////////

    getVaccineListByBookId(data) {
        return new Promise((resolve, reject) => {
            this.get(`/vaccine/list`, data).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getVaccineById(id) {
        return new Promise((resolve, reject) => {
            this.get(`/vaccine/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateVaccine(payload) {
        return new Promise((resolve, reject) => {
            this.update('/vaccine/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    deleteVaccine(payload) {
        return new Promise((resolve, reject) => {
            this.delete(`/vaccine/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getVaccineList = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/vaccine/list/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

export default VaccineProvider