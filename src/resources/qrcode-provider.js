import HttpRequest from './http-request'

class QRCodeProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    generate(data) {
        return new Promise((resolve, reject) => {
            this.create('/qrcode/generate', data).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getQRCodeSetList(data) {
        return new Promise((resolve, reject) => {
            this.get('/qrcode/get-qrcode-set-list', data).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getQRCodeItemList(data) {
        return new Promise((resolve, reject) => {
            this.get('/qrcode/get-qrcode-item-list', data).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getQRCodeSetById(id) {
        return new Promise((resolve, reject) => {
            this.get(`/qrcode/get-qrcode-set-by-id/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    downloadPDF(id) {
        return new Promise((resolve, reject) => {
            this.get(`/qrcode/download-pdf/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateQRCodeSet(data) {
        return new Promise((resolve, reject) => {
            this.update('/qrcode/update-qrcode-set', data).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getQrcodeInfo = () => {
        return new Promise((resolve, reject) => {
            this.get('/qrcode/get-qrcode-info').then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

export default QRCodeProvider