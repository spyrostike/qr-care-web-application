import Vue from 'vue'
import Router from 'vue-router'

import store from '../store'

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters['admin/isAuthenticated']) {
        next()
        return
    }

    next('/admin')
}

const ifAuthenticated = async (to, from, next) => {

    if (store.getters['admin/isAuthenticated']) {
        next()
        return
    }
    next('/admin/login')
}

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        { 
            path: '/admin/login', 
            component: () => import('@/views/admin/form/Login'), 
            name: 'admin-login',
            beforeEnter: ifNotAuthenticated 
        },
        // { path: '/', redirect: { name: 'dashboard' } },
        {
            path: '/admin',
            name: 'dashboard',
            component: () => import('@/views/layout/Admin'),
            meta: { title: 'Home' },
            beforeEnter: ifAuthenticated,
            children: [
                { path: '/', redirect: { name: 'qrcode' } },
                { name: 'profile', path: 'profile', component: () => import('@/views/admin/profile/Info'), meta: { title: 'Profile' } },
                // { name: 'dashboard', path: 'dashboard', component: () => import('@/views/dashboard/Dashboard'), meta: { title: 'Dashboard' } },
                { name: 'member', path: 'member', component: () => import('@/views/admin/member/Index'), meta: { title: 'Member' } },
                { 
                    name: 'member',
                    path: 'member', 
                    component: () => import('@/router/template/EmptyRouterView.vue'), 
                    meta: { title: 'Member' },
                    children: [
                        { 
                            path: 'edit/:id', 
                            component: () => import('@/views/admin/member/Info'), 
                            name: 'member-edit', 
                            meta: { title: 'Member Information', subtitle: 'Edit' } 
                        }
                    ]
                },
                { name: 'qrcode', path: 'qrcode', component: () => import('@/views/admin/qrcode-set/Index'), meta: { title: 'QRCode Set' } },
                { 
                    name: 'qrcode',
                    path: 'qrcode', 
                    component: () => import('@/router/template/EmptyRouterView.vue'), 
                    meta: { title: 'QRCode Set' },
                    children: [
                        { 
                            path: 'add', 
                            component: () => import('@/views/admin/qrcode-set/Info'), 
                            name: 'qrcode-add', 
                            meta: { title: 'QRCode Set Information', subtitle: 'Add' } 
                        },
                        { 
                            path: 'edit/:id', 
                            component: () => import('@/views/admin/qrcode-set/Info'), 
                            name: 'qrcode-edit', 
                            meta: { title: 'QRCode Set Information', subtitle: 'Edit' } 
                        }
                    ]
                },
                { name: 'qrcode-item', path: 'qrcode-item', component: () => import('@/views/admin/qrcode-item/Index'), meta: { title: 'QRCode Item' } },
                { name: 'creature', path: 'creature', component: () => import('@/views/admin/creature/Index'), meta: { title: 'Creature' } },
                { 
                    name: 'creature',
                    path: 'creature', 
                    component: () => import('@/router/template/EmptyRouterView.vue'), 
                    meta: { title: 'Creature' },
                    children: [
                        { 
                            path: 'edit/:id', 
                            component: () => import('@/views/admin/creature/Info'), 
                            name: 'creature-edit', 
                            meta: { title: 'Creature Information', subtitle: 'Edit' },
                        },
                        { 
                            path: 'edit/:id', 
                            component: () => import('@/router/template/EmptyRouterView.vue'), 
                            name: 'creature-edit', 
                            meta: { title: 'Creature Information', subtitle: 'Edit' },
                            children: [
                                { 
                                    path: 'treatment-history', 
                                    component: () => import('@/views/admin/treatment-history/Index.vue'), 
                                    name: 'treatment-history', 
                                    meta: { title: 'Treatment history' }
                                },
                                { 
                                    path: 'treatment-history', 
                                    component: () => import('@/router/template/EmptyRouterView.vue'), 
                                    name: 'treatment-history', 
                                    meta: { title: 'Treatment history' },
                                    children: [
                                        { 
                                            path: 'edit/:treatment_history_id', 
                                            component: () => import('@/views/admin/treatment-history/Info'), 
                                            name: 'treatment-history-edit', 
                                            meta: { title: 'Treatment history', subtitle: 'Edit' } 
                                        }
                                    ]
                                },
                                { 
                                    path: 'medical-info', 
                                    component: () => import('@/views/admin/medical-info/Info.vue'), 
                                    name: 'medical-info', 
                                    meta: { title: 'Medical info', subtitle: 'Edit' }
                                },
                                { 
                                    path: 'drug', 
                                    component: () => import('@/views/admin/drug/Index.vue'), 
                                    name: 'drug', 
                                    meta: { title: 'Drug' }
                                },
                                { 
                                    path: 'drug', 
                                    component: () => import('@/router/template/EmptyRouterView.vue'), 
                                    name: 'drug', 
                                    meta: { title: 'Drug' },
                                    children: [
                                        { 
                                            path: 'edit/:drug_id', 
                                            component: () => import('@/views/admin/drug/Info'), 
                                            name: 'drug-edit', 
                                            meta: { title: 'Drug', subtitle: 'Edit' } 
                                        }
                                    ]
                                },
                                { 
                                    path: 'drug-allergy', 
                                    component: () => import('@/views/admin/drug-allergy/Index.vue'), 
                                    name: 'drug-allergy', 
                                    meta: { title: 'Drug allergy' }
                                },
                                { 
                                    path: 'drug-allergy', 
                                    component: () => import('@/router/template/EmptyRouterView.vue'), 
                                    name: 'drug-allergy', 
                                    meta: { title: 'Drug allergy' },
                                    children: [
                                        { 
                                            path: 'edit/:drug_allergy_id', 
                                            component: () => import('@/views/admin/drug-allergy/Info'), 
                                            name: 'drug-allergy-edit', 
                                            meta: { title: 'Drug allergy', subtitle: 'Edit' } 
                                        }
                                    ]
                                },
                                { 
                                    path: 'food-allergy', 
                                    component: () => import('@/views/admin/food-allergy/Index.vue'), 
                                    name: 'food-allergy', 
                                    meta: { title: 'Food Allergy' }
                                },
                                { 
                                    path: 'food-allergy', 
                                    component: () => import('@/router/template/EmptyRouterView.vue'), 
                                    name: 'food-allergy', 
                                    meta: { title: 'Food allergy' },
                                    children: [
                                        { 
                                            path: 'edit/:food_allergy_id', 
                                            component: () => import('@/views/admin/food-allergy/Info'), 
                                            name: 'food-allergy-edit', 
                                            meta: { title: 'Food allergy', subtitle: 'Edit' } 
                                        }
                                    ]
                                },
                                { 
                                    path: 'vaccine-book', 
                                    component: () => import('@/views/admin/vaccine-book/Index.vue'), 
                                    name: 'vaccine-book', 
                                    meta: { title: 'Vaccine Book' }
                                },
                                { 
                                    path: 'vaccine-book', 
                                    component: () => import('@/router/template/EmptyRouterView.vue'), 
                                    name: 'vaccine-book', 
                                    meta: { title: 'Vaccine Book' },
                                    children: [
                                        { 
                                            path: 'edit/:vaccine_book_id', 
                                            component: () => import('@/views/admin/vaccine-book/Info'), 
                                            name: 'vaccine-book-edit', 
                                            meta: { title: 'Vaccine Book', subtitle: 'Edit' } 
                                        }
                                    ]
                                },
                                { 
                                    path: 'vaccine: _id', 
                                    component: () => import('@/router/template/EmptyRouterView.vue'), 
                                    name: 'vaccine-edit', 
                                    meta: { title: 'Vaccine' },
                                    children: [
                                        { 
                                            path: 'edit/:vaccine_id', 
                                            component: () => import('@/views/admin/vaccine/Info.vue'), 
                                            name: 'vaccine-edit', 
                                            meta: { title: 'Vaccine', subtitle: 'Edit' } 
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                { path: '*', redirect: { name: 'dashboard' } }
            ]
        },
        {
            path: '/creature',
            name: 'creature',
            component: () => import('@/views/layout/Creature'),
            meta: { title: 'Creature' },
            children: [
                { path: ':creature_id', component: () => import('@/views/creature/creature/Index.vue'), name: 'creature' },
                { 
                    path: ':creature_id', 
                    component: () => import('@/router/template/EmptyRouterView.vue'), 
                    name: 'creature',
                    children: [
                        { 
                            path: 'drug', 
                            component: () => import('@/views/creature/drug/Index'), 
                            name: 'drug'
                        },
                        { 
                            path: 'drug/:drug_id', 
                            component: () => import('@/views/creature/drug/Info'), 
                            name: 'drug-info'
                        },
                        { 
                            path: 'drug-allergy', 
                            component: () => import('@/views/creature/drug-allergy/Index'), 
                            name: 'drug-allergy'
                        },
                        { 
                            path: 'drug-allergy/:drug_allergy_id', 
                            component: () => import('@/views/creature/drug-allergy/Info'), 
                            name: 'drug-allergy-info'
                        },
                        { 
                            path: 'food-allergy', 
                            component: () => import('@/views/creature/food-allergy/Index'), 
                            name: 'food-allergy'
                        },
                        { 
                            path: 'food-allergy/:food_allergy_id', 
                            component: () => import('@/views/creature/food-allergy/Info'), 
                            name: 'food-allergy-info'
                        },
                        { 
                            path: 'medical', 
                            component: () => import('@/views/creature/medical/Index'), 
                            name: 'medical'
                        },
                        { 
                            path: 'treatment-history', 
                            component: () => import('@/views/creature/treatment-history/Index'), 
                            name: 'treatment-history'
                        },
                        { 
                            path: 'treatment-history/:treatment_history_id', 
                            component: () => import('@/views/creature/treatment-history/Info'), 
                            name: 'treatment-history-info'
                        },
                        { 
                            path: 'treatment-history-image-list/:treatment_history_id', 
                            component: () => import('@/views/creature/treatment-history/ImageList'), 
                            name: 'treatment-history-image-list'
                        },
                        { 
                            path: 'parent/:parent_id', 
                            component: () => import('@/views/creature/parent/Index'), 
                            name: 'parent'
                        },
                        { 
                            path: 'location/:parent_id', 
                            component: () => import('@/views/creature/location/Index'), 
                            name: 'parent-info'
                        },
                        { 
                            path: 'vaccine-book', 
                            component: () => import('@/views/creature/vaccine-book/Index'), 
                            name: 'vaccine-book'
                        },
                        { 
                            path: 'vaccine-book/:vaccine_book_id', 
                            component: () => import('@/views/creature/vaccine-book/Info'), 
                            name: 'vaccine-book-info'
                        },
                        { 
                            path: 'vaccine/:vaccine_id', 
                            component: () => import('@/views/creature/vaccine/Info'), 
                            name: 'vaccine-info'
                        }
                    ]
                }
            ]
        },
        {
            path: '*',
            component: () => import('@/views/pages/Index'),
            children: [
                {
                    name: '404 Error',
                    path: '',
                    component: () => import('@/views/pages/Error')
                }
            ]
        }
    ]
})
